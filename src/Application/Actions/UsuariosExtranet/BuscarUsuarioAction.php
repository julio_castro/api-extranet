<?php


namespace App\Application\Actions\UsuariosExtranet;


use App\Application\Actions\Action;
use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\Usuarios\UsuarioExtranetOrmService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

class BuscarUsuarioAction extends Action
{
    protected UsuarioExtranetOrmService $usuarioExtranetOrmService;

    /**
     * BuscarUsuarioAction constructor.
     * @param LoggerInterface $logger
     * @param UsuarioExtranetOrmService $usuarioExtranetOrmService
     */
    public function __construct(LoggerInterface $logger, UsuarioExtranetOrmService $usuarioExtranetOrmService)
    {
        parent::__construct($logger);
        $this->usuarioExtranetOrmService = $usuarioExtranetOrmService;
    }

    /**
     * @inheritDoc
     */
    protected function action(): Response
    {
        // TODO: Implement action() method.
        $idUsuario = $this->resolveArg('id');

        $usuario = $this->usuarioExtranetOrmService->getUsuario(strval($idUsuario));

//        echo "<pre>";
//        var_dump($usuario);
//        echo "</pre>";
//
//        exit();

        $dados = [
                    "id_usuario" => $usuario->getId(),
                    "nome"       => utf8_encode($usuario->getNachname()),
                    "email"      => $usuario->getEmail()
                 ];


        return $this->respondWithData($dados);
    }
}