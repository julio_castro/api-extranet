<?php


namespace App\Application\Actions\UsuariosExtranet;


use App\Application\Actions\Action;
use App\Domain\Usuarios\UsuarioExtranetOrmService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class ListarUsuariosAction extends Action
{
    protected UsuarioExtranetOrmService $usuarioExtranetOrmService;

    /**
     * ListarUsuariosAction constructor.
     * @param LoggerInterface $logger
     * @param UsuarioExtranetOrmService $usuarioExtranetOrmService
     */
    public function __construct(LoggerInterface $logger, UsuarioExtranetOrmService $usuarioExtranetOrmService)
    {
        parent::__construct($logger);
        $this->usuarioExtranetOrmService = $usuarioExtranetOrmService;
    }


    /**
     * @inheritDoc
     */
    protected function action(): Response
    {
        // TODO: Implement action() method.

        $usuarios = $this->usuarioExtranetOrmService->getUsuariosList();

        $dados = [];

        foreach($usuarios as $usuario){
            $dados[] = [
                'id' => $usuario->getId(),
                'nome' => utf8_encode($usuario->getNachname()),
                'email' => $usuario->getEmail(),
                'login' => $usuario->getKurz(),
            ];
        }



        return $this->respondWithData($dados);
    }
}