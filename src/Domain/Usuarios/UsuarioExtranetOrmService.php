<?php


namespace App\Domain\Usuarios;


use Doctrine\ORM\EntityManagerInterface;

class UsuarioExtranetOrmService
{
    protected EntityManagerInterface $em;

    /**
     * UsuarioExtranetOrmService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return UsuarioExtranetEntity[]|object[]|null
     */
    public function getUsuariosList()
    {
        return $this->em->getRepository(UsuarioExtranetEntity::class)->findAll();
    }

    /**
     * @param string $id
     * @return UsuarioExtranetEntity|object|null
     */
    public function getUsuario(string $id)
    {
        return $this->em->getRepository(UsuarioExtranetEntity::class)->find($id);
    }
}