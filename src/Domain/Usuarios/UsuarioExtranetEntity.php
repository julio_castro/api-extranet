<?php


namespace App\Domain\Usuarios;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UsuarioExtranetEntity
 * @package App\Domain\Usuarios
 * @ORM\Entity()
 * @ORM\Table("usuarios")
 */
class UsuarioExtranetEntity
{
    /**
     * @ORM\Id
     * @ORM\Column (name="ID", type="string", length=200, nullable=false)
     */
    protected string $id;
    /**
     * @ORM\Column (name="nachname", type="string", length=200, nullable=false)
     */
    protected string $nachname;
    /**
     * @ORM\Column (name="kurz", type="string", length=200, nullable=false)
     */
    protected string $kurz;
    /**
     * @ORM\Column (name="pw", type="string", length=200, nullable=false)
     */
    protected string $pw;
    /**
     * @ORM\Column (name="email", type="string", length=200, nullable=false)
     */
    protected string $email;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNachname(): string
    {
        return $this->nachname;
    }

    /**
     * @return string
     */
    public function getKurz(): string
    {
        return $this->kurz;
    }

    /**
     * @return string
     */
    public function getPw(): string
    {
        return $this->pw;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }


}